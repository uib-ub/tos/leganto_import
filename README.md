# Import of local course data to Alma
This repository contains code, workflows and relevant information pertaining to the import of course data to our library system, [Alma](https://www.proquest.com/products-services/Ex-Libris-Alma.html).

## The problem
In 2018, the University of Bergen acquired the digital reading list system [Leganto](https://www.exlibrisgroup.com/products/leganto-course-resource-list-management/) which is a producted offered by Ex Libris, whom also offers Alma.
Leganto is tightly integrated with Alma, it is in fact essentially a plugin to Alma which lets users create, manage and view reading lists. As of 2018 there exists over 2600 courses at the University of Bergen, according to UiBs [own website](https://www.uib.no/en/course).
Although there are probably some inactive courses in that count, over 2000 courses were expected to have digital reading lists in 2018/2019. Leganto does not hold any data of its own, all data stems from Alma.
But prior to 2018 it had not been neccessary to have course data from the university in the library system. In order to ensure that reading lists were affiliated with the correct courses and that the people affiliated with the courses were
able to work with the reading lists in Leganto to name a few reasons, the course data had to exist in Alma.

## The solution
Alma has a so called "integration profile" which facilitates interaction between Alma and external systems. Alma also supports the set up of a "Course loader" which makes the relevant integration easier. See the documentation for the Course loader <a href="https://knowledge.exlibrisgroup.com/Alma/Product_Documentation/010Alma_Online_Help_(English)/090Integrations_with_External_Systems/040Fulfillment/010Courses_and_Reading_Lists/Configuring_Course_Loading">here</a>.
In short, this enables us to upload a tab separated .txt file to a FTP server which Alma then can harvest.

The data is provided by the study administration system, [FS](https://www.fellesstudentsystem.no/english/) in XML. This data is automatically fetched via HTTPS and massaged by the script in this repository. The ouput is stored in an Alma accessible FTP server. Alma is configured to harvest this directory automatically, everyday at 09:00.

### Snippet of .txt file

```
course_code	course_title	section_id	acad_dept	proc_dept	term1	term2	term3	term4	start_date	end_date	num_of_participants	weekly_hours	year	search_id1	search_id2	search_id3	instr1	instr2	instr3	instr4	instr5	instr6	instr7	instr8	instr9	instr10	all_instructors	operation	old_course_code	old_course_section
2019V-SAMPOL338-0	Breaking BAD: Understanding Backlash Against Democracy		184151300	320001	SPRING				01-01-2019	30-06-2019	0																3412@uib.no,4532@uib.no,3378@uib.no,2221@uib.no,1314@uib.no
2019V-SAMPOL339-0	Democratic Representation: Elections, Parliaments, Responsiveness		184151300	320001	SPRING				01-01-2019	30-06-2019	0																1083@uib.no
2019V-KMD-VIS-233-0	Fordypning visuell kommunikasjon		184182000	320007	SPRING				01-01-2019	30-06-2019	0
2019V-ELTE-EKS-0	Avsluttende vurdering		184170000	320006	SPRING				01-01-2019	30-06-2019	0																0943@uib.no,5468@uib.no
2019V-KMD-TEK-216-0	Construction & Context 2 - Digital Weaving		184181000	320007	SPRING				01-01-2019	30-06-2019	0
```

# Setup
This projects runs on Python 3 and has only one not-OOTB Python library, which is [Requests](https://pypi.org/project/requests/).

Run these commands on a clean Centos 7 server to get the code to run:

`yum install epel-release`

`yum install python3`

`pip3 install --user requests`

Then cd to your desired directory and clone this repo:

`git clone git@git.app.uib.no:uib-ub/tos/leganto_import.git`

Then cd to the directory named leganto_import and make the two following directories which are used by the project:

`mkdir staging to_be_imported`

# Initial import

After you have provided the correct parameters and credentials in [config.json](config.example.json), get the first import by running:

`python3 get_fs-data.py`

This will create a file called importData.txt in the to_be_imported directory if said directory was empty at the time the script was ran.

# Updating courses (Rollover issues)

Sometimes it might be necessary to import course data several times for a semester. This need might arise as a consequence of some courses not being made in time for the deadline, or some courses being updated after the deadline.

Because we specify which operation should be carried out on each course, we have to tread somewhat lightly when we do updates. An operation specifies whether the course should be updated, deleted or copied (rollover in Alma parlance).

Our initial import has many records with the rollover operation specified. If another import is done with the rollover operation specified again, a duplicated course would be created which is undesirable as it leads to alot of confusion for the list owners and librarians alike.

In order to avoid this, this repo has a script called:

`get_rolledover_courses.py`

This script finds all previous rolled over courses and changes their operation to update which only updates the course with any new information.

In order to run this script, `get_fs-data.py` must have been run twice.

# Important: Changing year/semester
What year/semester is harvested is determined in a file on the server called `config.json`. This file holds the URL to fs, along with the semester to be harvested. It also holds the token which lets us use the FS service:

`{
  "DEFAULT": {
    "URL": "https://fs.fd.uib.no/leganto_undervisningsenheter/2021/",
    "semester": "høst",
    "FS_TOKEN": "FS_token_here"
  }
}
`

So when you want a new semester to be harvested, this file needs to be changed on the server. You might need to change the year in the URL itself, and the semester.

# Common pitfalls

Because the data is entered manually into FS, errors occur. A common error is that there is an extra carriage return typed into the course name in FS. This breaks the formatting of our import file which expects a course on each line. The offending carriage return needs to be removed before import. The program could be expanded to tackle issues like these automatically.

# Setup in Alma

In order to import the data into Alma, some setup is needed. Alma needs to know the IP, directory and credentials to where it should get the data. This setup is documented by Ex Libris [here](https://knowledge.exlibrisgroup.com/Alma/Product_Documentation/010Alma_Online_Help_(English)/090Integrations_with_External_Systems/040Fulfillment/010Courses_and_Reading_Lists/Configuring_Course_Loading).
