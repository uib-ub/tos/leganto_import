#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import json

#Initial setup with FS URL and FS token
with open('config.json', 'r') as cfg:
    config = json.load(cfg)
    
def getToken():    
    token = config['DEFAULT']['FS_TOKEN']
    return(token)

def getURL():
    url = config['DEFAULT']['URL'] + config['DEFAULT']['semester']
    return(url)


#This function takes care of removing previous files from the directory as Alma only accepts one file.
def clean_directory(directory):
    
    if len(os.listdir(directory)) != 0:
        os.remove(os.path.join(directory, os.listdir(directory)[0]))
        clean_directory(directory)
    else:
        return(True)

def get_filename(directory):
    
    for file in os.listdir(directory):
        return file