#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import xml.etree.ElementTree as ET
import requests as r
import csv
import re
import sys
import os
from helper_functions import clean_directory, get_filename, getToken, getURL 


def get_fsData():
    url = getURL()
    token = getToken()
    headers = {'authorization': 'Bearer {}'.format(token)}
    fs_data = r.get(url, headers=headers)
    fs_data.encoding = 'utf-8'
    fs_data_encoded = fs_data.text.encode('utf-8')

    
    #Check that status and headers are OK, if not - raise error with status code
    if fs_data.status_code == 200 and fs_data.headers['Content-type'] == 'text/xml;charset=UTF-8':
        remove_ampersand(fs_data.text)
    elif fs_data.status_code == 404:
        fs_data.raise_for_status()


#This function reads the variable with the xml-data and parses it with the ElementTree library. It sends the data on to the function creating the file
def open_xml(raw_xml):
    root = ET.fromstring(raw_xml);
    create_file_for_import(root)


#This function takes the fs_data, loops through it to find any un-encoded ampersands and encodes them properly. After this it sends the xml as a variable to open_xml
def remove_ampersand(fs_data):
    for x in fs_data:
      if '&' in x:
         x.replace('&', '&amp;')
    open_xml(fs_data)

#This function is responsible for creating the .csv which will be imported into Alma. We also clean the directory of previous files before we create a new one
def create_file_for_import(root):
        
    # open a file for writing. If the initial import for the semester, write directly to to_be_imported-directory, otherwise write to staging-directory
    if len(os.listdir('to_be_imported')) == 0:
        fs_data = open('to_be_imported/importData.txt', 'w', encoding='utf-8-sig')
    else:
        clean_directory('staging')
        fs_data = open('staging/stagingData.txt', 'w', encoding='utf-8-sig')
    # create the csv writer object
    csvwriter = csv.writer(fs_data, dialect='excel-tab')
    fsdata_head = []
    count = 0

    #add all headers to the fsdata_head variable which is then written as the header in the excel-tab dialect by the DictWriter.
    for member in root[0].iter():
        #Remove the header 'undervisningsenhet' which is not used in Alma
        if member.tag != 'undervisningsenhet':
            fsdata_head.append(member.tag)
    dwriter = csv.DictWriter(fs_data, fieldnames=fsdata_head, dialect='excel-tab', extrasaction='ignore')
    dwriter.writeheader()

    #Write rows
    headers = 0;
    rowToWrite = {}
    if headers < len(fsdata_head):
        for row in root:
            dwriter.writerow({row[0].tag: row[0].text, row[1].tag: row[1].text, row[2].tag: row[2].text, row[3].tag: row[3].text, row[4].tag: row[4].text, row[5].tag: row[5].text, row[6].tag: row[6].text, row[7].tag: row[7].text, row[8].tag: row[8].text, row[9].tag: row[9].text, row[10].tag: row[10].text, row[11].tag: row[11].text, row[12].tag: row[12].text, row[13].tag: row[13].text, row[14].tag: row[14].text, row[15].tag: row[15].text, row[16].tag: row[16].text, row[17].tag: row[17].text, row[18].tag: row[18].text, row[19].tag: row[19].text, row[20].tag: row[20].text, row[21].tag: row[21].text, row[22].tag: row[22].text, row[23].tag: row[23].text, row[24].tag: row[24].text, row[25].tag: row[25].text, row[26].tag: row[26].text, row[27].tag: row[27].text, row[28].tag: row[28].text, row[29].tag: row[29].text, row[30].tag: row[30].text})
        headers = headers + 1
    fs_data.close()


get_fsData()

