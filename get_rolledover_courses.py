#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import xml.etree.ElementTree as ET
import requests as r
import csv
import re
import sys
import tempfile
import os
from helper_functions import clean_directory, get_filename


#This function gets the course codes of the previously imported file, and stores them in a list
def get_old_course_codes():
    
    courses = []
        
    for file in os.listdir('to_be_imported'):
        with open('to_be_imported/{}'.format(file), encoding='utf-8-sig') as fd:
            rd = csv.reader(fd, delimiter='\t', quotechar='"')
            for row in rd:
                if row[28] == 'UPDATE' or 'ROLLOVER':
                    courses.append(row[0])
        return(courses)
    
    
#This function gets the course codes that are to be ROLLED over in a not previously imported file, i.e a 'new' file.
def get_new_course_codes():
    
    rollover_courses_to_be_imported = []
    
    for new_file in os.listdir('staging'):
        with open('staging/{}'.format(new_file), encoding='utf-8-sig') as fd:
            rd = csv.reader(fd, delimiter='\t', quotechar='"')
            for row in rd:
                if row[28] == 'ROLLOVER':
                    rollover_courses_to_be_imported.append(row[0])
        return(rollover_courses_to_be_imported)

#We compare the 'old' course codes with the 'new' course codes. If there is a match, this means that the course has previously been rolled over. Previously rolled over course codes are stored in a new list.
def match_course_codes():
    
    old_courses = get_old_course_codes()
    new_courses = get_new_course_codes()
    
    courses_to_update = []
    
    for course_code in old_courses:
        if course_code in new_courses:
            courses_to_update.append(course_code)
    overwrite_status_to_update(courses_to_update)

#This function overwrites the operation field (value: ROLLOVER) to 'value: UPDATE' if a course code is matched with a course in the courses_to_update list
def overwrite_status_to_update(courses_to_update):
    
    staging_file = get_filename('staging')
        
    in_file = open('staging/{}'.format(staging_file), 'r+', encoding='utf-8-sig')    
    reader = csv.reader(in_file, delimiter='\t')
    
    
    #Write a new import file
    clean_directory('to_be_imported') 
    out_file = open('to_be_imported/importData.txt', 'w+', encoding='utf-8-sig')
    
    writer = csv.writer(out_file, delimiter='\t')
    
    
    for row in reader:
        if row[0] in courses_to_update:
            row[28] = 'UPDATE'
            writer.writerow(row)
        else:
            writer.writerow(row)
    in_file.close()    
    out_file.close()
                 
match_course_codes()